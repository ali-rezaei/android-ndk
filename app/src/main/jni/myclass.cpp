// my second program in C++

#include <iostream>
#include <string.h>
#include <jni.h>

using namespace std;

extern "C" {
   JNIEXPORT jstring JNICALL Java_com_example_android_jni_MainActivity_helloJNI(JNIEnv * env, jobject obj)
   {
      string str = "Tjena from Ali in C++ over JNI";
      return env->NewStringUTF(str.c_str());
   }
}
