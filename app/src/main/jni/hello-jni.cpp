#include <string.h>
#include <jni.h>
#include <iostream>

extern "C" {
      JNIEXPORT jstring JNICALL Java_com_example_android_jni_MainActivity_stringFromJNI
      (JNIEnv * env, jobject obj)
      {
            return env->NewStringUTF("Hello from C++ over JNI!");
      }
}